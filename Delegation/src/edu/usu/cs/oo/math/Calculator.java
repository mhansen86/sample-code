package edu.usu.cs.oo.math;

public interface Calculator {
	
	public int add(int a, int b);

	public int subtract(int a, int b);
	public int multiply(int a, int b);
	public int mod(int a, int b);

}
