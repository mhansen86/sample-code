import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;


public class Driver {
	
	public static void main(String[] args)
	{
		FootballPlayer fp1 = new FootballPlayer(20, 454*190, 2, 100, "Steve McFootball");
		FootballPlayer fp2 = new FootballPlayer(90, 454*100, 1, 3521, "Anthony Calvillo");
		FootballPlayer fp3 = new FootballPlayer(21, 454*275, 10, -18, "Steve McFootball");
		FootballPlayer fp4 = new FootballPlayer(25, 454*250, 14, 105, "Beau Ziffer");
		FootballPlayer fp5 = new FootballPlayer(54, 454*390, 0, 0, "Linebacker McRoids");
		FootballPlayer fp6 = new FootballPlayer(4, 454*16, 3, 18, "Billy Little League");
		FootballPlayer fp7 = new FootballPlayer(13342, 454*100, 1, 1, "Leopoldus the Ancient");
		FootballPlayer fp8 = new FootballPlayer(23, 454*90, -4, 3, "Archibald the Punter");
		
		
		List<FootballPlayer> footballPlayerList = new LinkedList<FootballPlayer>();
		
		footballPlayerList.add(fp1);
		footballPlayerList.add(fp2);
		footballPlayerList.add(fp3);
		footballPlayerList.add(fp4);
		footballPlayerList.add(fp5);
		footballPlayerList.add(fp6);
		footballPlayerList.add(fp7);
		footballPlayerList.add(fp8);
		
		SelectionSort sorter = new SelectionSort();
		
		List<FootballPlayer> sortedList = sorter.sort(footballPlayerList, new Comparer(){

			@Override
			public int compareTo(FootballPlayer fp1, FootballPlayer fp2) {
				if (fp1.getWeightInGrams() < fp2.getWeightInGrams()) return -1;
				if (fp1.getWeightInGrams() > fp2.getWeightInGrams()) return 1;
				return 0;
			}
			
		});
		
		for (FootballPlayer fp : sortedList)
		{
			System.out.println(fp);
		}
		
		
		Collections.sort(footballPlayerList, new Comparator<FootballPlayer>() {

			@Override
			public int compare(FootballPlayer o1, FootballPlayer o2) {
				return -1;
			}
			
		});
		
	}

}
